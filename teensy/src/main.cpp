#include <ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Empty.h>
#include <sensor_msgs/JointState.h>
#include <Encoder.h>
#include <filters.h>


#define PUBLISH_PERIOD_MICRO 1000
#define MOTOR_COMMAND_TIMEOUT_MILLI 100     // stop motor if no new message has been received via "acrobot_duty_cycle" topic for more than this amount
#define PWM_MAX 4095
#define VOLTAGE_MAX 48.0f
#define CURRENT_MAX 15.0f
#define USE_MAXON 1
#define MAXON_PWM_OFF 2048

// Timing
elapsedMicros last_spin;
elapsedMillis last_command_received;


// Teensy's encoder library
Encoder encoders[3] = {
  Encoder(3, 2), // joint1
  Encoder(4, 5), // joint2_shoulder
  Encoder(6, 7), // joint2_elbow
};
const float encoder_resolutions[3] = {1024, 500*13/3, 500};
float old_positions[3] = {-999, -999, -999};


// Motor connections
const int pwma_pin = 22; 
const int ina1_pin = 21;
const int ina2_pin = 20;


// Lowpass filter
const float cutoff_freq   = 15.0;     // Cutoff frequency in Hz
const float sampling_time = PUBLISH_PERIOD_MICRO/1e6;    // Sampling time in seconds.
IIR::ORDER  order  = IIR::ORDER::OD3; // Order (OD1 to OD4)
Filter lpf[3] = {
  Filter(20.0, sampling_time, order),
  Filter(cutoff_freq, sampling_time, order),
  Filter(cutoff_freq, sampling_time, order)
};


// ROS message
sensor_msgs::JointState jstate;
char *_jstate_name[] = {"joint1", "joint2_shoulder", "joint2_elbow"};
float _jstate_pos[3] = {0,0,0};
float _jstate_vel[3] = {0,0,0};
float _jstate_eff[3] = {0,USE_MAXON ? MAXON_PWM_OFF : 0,0};


// Helper functions for writing motor commands
template<class T>
constexpr const T& clamp( const T& v, const T& lo, const T& hi )
{
    return (v < lo) ? lo : (hi < v) ? hi : v;
}
template<class T>
T& map( T& x, T& in_min, T& in_max, T& out_min, T& out_max )
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


// Motor commanding functions
int torque_to_dc( const float torque )
{
    float gear_ratio = 13.0f / 3.0f;
    float eta = 0.92;      
    float k_tau = 60.3e-3;             // 60.3 mN-m/A to N-m/a
    float k_emf = 1./(158*2*M_PI/60);  // convert 158 rpm/V to rad/s
    float R = 1.13;                    // Ohms
    float I = torque / gear_ratio / eta / k_tau;

    int dc;
    if (USE_MAXON) {
      /*** Current out - for use with Maxon ESCON */
      dc = ceilf( map(clamp(I,-CURRENT_MAX,CURRENT_MAX), -CURRENT_MAX, CURRENT_MAX, PWM_MAX*0.1, PWM_MAX*0.9) );
    } else{
      /*** Voltage out - for use with Pololu H-bridge */
      float motor_speed = _jstate_vel[1] * gear_ratio;
      float V = I*R + k_emf*motor_speed;
      dc = ceilf( clamp(V, -VOLTAGE_MAX, VOLTAGE_MAX) / VOLTAGE_MAX * PWM_MAX );
    }
    
    return dc;
}
void motor_brake()
{
  if (USE_MAXON) {
    analogWrite(pwma_pin, MAXON_PWM_OFF);
  } else {
    digitalWrite(pwma_pin, LOW);
    digitalWrite(ina1_pin, HIGH);
    digitalWrite(ina2_pin, HIGH);
  }
}
void motor_reset()
{
  if (USE_MAXON) {
    analogWrite(pwma_pin, MAXON_PWM_OFF);
    digitalWrite(ina1_pin, LOW);
  } else {
    digitalWrite(pwma_pin, LOW);
    digitalWrite(ina1_pin, LOW);
    digitalWrite(ina2_pin, LOW);
  }
}
void motor_coast()
{
  if (USE_MAXON) {
    analogWrite(pwma_pin, MAXON_PWM_OFF);
  } else {
    digitalWrite(pwma_pin, HIGH);
    digitalWrite(ina1_pin, LOW);
    digitalWrite(ina2_pin, LOW);
  }
}
void motor_cb(const std_msgs::Float32& cmd_msg)
{
  last_command_received = 0;  // reset timeout
  float torque = cmd_msg.data;
  int duty_cycle = torque_to_dc(torque);
  _jstate_eff[0] = torque;
  _jstate_eff[1] = duty_cycle;

  if (USE_MAXON) {
    if (abs(duty_cycle - MAXON_PWM_OFF) < 0.005*PWM_MAX) {
      motor_coast();
    } else {
      digitalWrite(ina1_pin, HIGH);
      analogWrite(pwma_pin, duty_cycle);
    }
  } else {
    if (duty_cycle < 0) {
      digitalWrite(ina1_pin, HIGH);
      digitalWrite(ina2_pin, LOW);
      analogWrite(pwma_pin, abs(duty_cycle));
    } else if (duty_cycle > 0) {
      digitalWrite(ina1_pin, LOW);
      digitalWrite(ina2_pin, HIGH);
      analogWrite(pwma_pin, duty_cycle);
    } else if (duty_cycle == 0) {
      motor_coast();
    } 
  }
}
void reset_cb(const std_msgs::Empty& reset_msg)
{
  for (int i=0; i<3; i++) {
    encoders[i].write(0);
  }
}

// ROS node
ros::NodeHandle nh;
ros::Publisher  joint_state_publisher("acrobot_state", &jstate);
ros::Subscriber<std_msgs::Float32> command_subscriber("acrobot_command", &motor_cb);
ros::Subscriber<std_msgs::Empty>   reset_subscriber("acrobot_reset", &reset_cb);


void setup()
{
  if (USE_MAXON) {
    analogWriteFrequency(pwma_pin, 4000);
  } else {
    analogWriteFrequency(pwma_pin, 20000);
  }
  analogWriteResolution(12);
  pinMode(pwma_pin, OUTPUT);
  pinMode(ina1_pin, OUTPUT);
  pinMode(ina2_pin, OUTPUT);
  motor_reset();
  
  nh.initNode();
  jstate.name            = _jstate_name;
  jstate.position        = _jstate_pos;
  jstate.velocity        = _jstate_vel;
  jstate.effort          = _jstate_eff;
  jstate.name_length     = 3;
  jstate.position_length = 3;
  jstate.velocity_length = 3;
  jstate.effort_length   = 3;
  nh.advertise(joint_state_publisher);
  nh.subscribe(command_subscriber);
  nh.subscribe(reset_subscriber);
}


void loop()
{ 

  if (last_spin >= PUBLISH_PERIOD_MICRO) {
    for (int i=0; i<3; i++) {
      _jstate_pos[i] = (float) encoders[i].read() / 4.0 / encoder_resolutions[i] * 2.0 * M_PI;

      float delta = ( _jstate_pos[i] - old_positions[i] ) / sampling_time;
      old_positions[i] = _jstate_pos[i];
      _jstate_vel[i] = lpf[i].filterIn(delta);

      jstate.position[i] = _jstate_pos[i];
      jstate.velocity[i] = _jstate_vel[i];
      jstate.effort[i] = _jstate_eff[i];
    }
    
    jstate.header.stamp = nh.now();
    joint_state_publisher.publish( &jstate );
    nh.spinOnce();
    last_spin -= PUBLISH_PERIOD_MICRO;
  }

  if (last_command_received >= MOTOR_COMMAND_TIMEOUT_MILLI) {
    _jstate_eff[0] = 0.0;
    if (USE_MAXON) {
      _jstate_eff[1] = MAXON_PWM_OFF;  
    } else {
      _jstate_eff[1] = 0;
    }
    if (last_command_received >= 2500) {
      motor_reset();
    } else {
      motor_brake();
    }
  }
  
}
