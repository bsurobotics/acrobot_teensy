#!/usr/bin/env python3

import ml_esc
from math import isclose
import timeit

def test_output():
    p = ml_esc.ML_ESC([6, 128, 128, 1])
    p.load_weights()

    p.set_state([-0.83775806, -0.20943952, 0.41243276, 0.3908176])
    print('Expected 0.7653280497575692, got {0:.16f}'.format(p.get_torque()))
    assert isclose(p.get_torque(), 0.7653280497575692, abs_tol=1e-5)

    p.set_state([0.8726646, 0.22689278, -0.7995896, 0.7293835])
    print('Expected -1.6818195497328916, got {0:.16f}'.format(p.get_torque()))
    assert isclose(p.get_torque(), -1.6818195497328916, abs_tol=1e-5)

def test_gradient_execution_time():
    p = ml_esc.ML_ESC([6, 128, 128, 1])
    p.load_weights()
    p.set_state([-0.83775806, -0.20943952, 0.41243276, 0.3908176])

    elapsed = timeit.timeit(lambda: p.net.gradient(p.input), number=10000) / 10000.0
    print("average time to compute NN gradient (loop):     {0:.6f} sec".format(elapsed))

def test_gradient_hardcode_execution_time():
    p = ml_esc.ML_ESC([6, 128, 128, 1])
    p.load_weights()
    p.set_state([-0.83775806, -0.20943952, 0.41243276, 0.3908176])

    elapsed = timeit.timeit(lambda: p.net.gradient_hardcode(p.input), number=10000) / 10000.0
    print("average time to compute NN gradient (hardcode): {0:.6f} sec".format(elapsed))

def test_output_execution_time():
    p = ml_esc.ML_ESC([6, 128, 128, 1])
    p.load_weights()
    p.set_state([-0.83775806, -0.20943952, 0.41243276, 0.3908176])

    elapsed = timeit.timeit(lambda: p.get_torque(), number=10000) / 10000.0
    print("average time to compute torque:                 {0:.5f} sec".format(elapsed))


if __name__ == "__main__":
    test_output()
    test_gradient_execution_time()
    test_gradient_hardcode_execution_time()
    test_output_execution_time()
    