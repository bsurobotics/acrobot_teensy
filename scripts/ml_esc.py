from numpy.random import randn
from math import sin, cos, tanh, sqrt, exp, pi
import numpy as np


def identity(x):
    return x


def elu(x, alpha=None):
    if alpha is None: 
        alpha = 1.0
    alpha = type(x)(alpha)
    if x > 0:
        return x
    else:
        return alpha*(exp(x) - type(x)(1))


def delu(x, alpha=None):
    if alpha is None: 
        alpha = 1.0
    alpha = type(x)(alpha)
    if x > 0:
        return type(x)(1.0)
    else:
        return elu(x, alpha) + alpha


class DenseLayer(object):

    def __init__(self, nin, nout, sigma, initW=None, initb=None):
        self.nin = nin
        self.nout = nout
        self.sigma = np.vectorize(sigma)
        self.sigma_prime = np.vectorize(delu)
        if initW is None:
            self.W = ( randn(nout, nin)*sqrt(1.0/nin) ).astype(np.float32) 
        elif initW.shape == (nin, nout):
            self.W = np.asarray(initW, dtype=np.float32)
        else:
            raise IndexError('Weight matrix dimensions do not match those of input and/or output')
        if initb is None:
            self.b = randn(nout).astype(np.float32)
        elif initb.shape == (nin, nout):
            self.b = np.asarray(initb, dtype=np.float32)
        else:
            raise IndexError('Bias matrix dimensions do not match those of input and/or output')

    def set_W(self, W):
        self.W = np.asarray(W, dtype=np.float32)

    def set_b(self, b):
        self.b = np.asarray(b, dtype=np.float32)

    def output(self, input):
        return np.dot(self.W, np.float32(input)) + self.b

    def activation(self, input):
        return self.sigma(self.output(input))


class Net(object):

    def __init__(self, *layers):
        self.layers = layers

    def output(self, input):
        z = input
        for i in range(0, len(self.layers)):
            z = self.layers[i].activation(z)
        return z

    def gradient(self, input):
        grad = self.layers[-1].W
        activation = input
        zs = []
        for layer in self.layers[:-1]:
            z = layer.output(activation)
            zs.append(z)
            activation = layer.sigma(z)
            
        for i in range(2, len(self.layers)+1): 
            dsigma_eval = [ self.layers[-i].sigma_prime(zs[-i+1]) ]
            column_prod = np.multiply( 
                np.asarray( dsigma_eval , dtype=np.float32 ).T, 
                self.layers[-i].W
            ) 
            grad = np.dot(grad, column_prod)

        return grad

    def gradient_hardcode(self, input):
        z1 = np.dot(self.layers[0].W, input) + self.layers[0].b
        z2 = np.dot(self.layers[1].W, self.layers[1].sigma(z1)) + self.layers[1].b
        zs = [z1, z2]
        g1 = np.dot(
            self.layers[-1].W,
            np.multiply( 
                np.asarray([self.layers[-2].sigma_prime(zs[-1])], dtype=np.float32).T,
                self.layers[-2].W
            )
        )
        g2 = np.multiply( 
            np.asarray([self.layers[-3].sigma_prime(zs[-2])], dtype=np.float32).T,
            self.layers[-3].W
        )
        return np.dot(g1, g2)



class ML_ESC(object):

    def __init__(self, widths):
        depth = len(widths)-1
        layers = tuple(
            DenseLayer(widths[i], widths[i+1], identity if (i==depth-1) else elu) for i in range(depth)
        )
        self.net = Net(*layers)
        self.input = np.zeros(6)
        self.gradient_coeffs = np.zeros(4)
        self.torque = 0.0

    def load_weights(self):
        i = 1
        for layer in self.net.layers:
            layer.set_W(np.load('/home/wankunsirichotiyakul/ros_catkin/acrobot/src/acrobot_teensy/scripts/nn_params/W' + str(i) + '.npy'))
            layer.set_b(np.load('/home/wankunsirichotiyakul/ros_catkin/acrobot/src/acrobot_teensy/scripts/nn_params/b' + str(i) + '.npy'))
            i += 1
        self.gradient_coeffs = np.load('/home/wankunsirichotiyakul/ros_catkin/acrobot/src/acrobot_teensy/scripts/nn_params/gradient_coeffs.npy')

    def compute_control(self):
        # nabla_x = self.net.gradient(self.input)
        nabla_x = self.net.gradient_hardcode(self.input)
        A = np.array([
            [1, 1, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0]
        ], dtype=np.float32)
        B = np.multiply(
            np.multiply(
                nabla_x, 
                self.input[[1,0,2,4,3,5]]
            ),
            np.array([-1, 1, 0, -1, 1, 0], dtype=np.float32)
        )
        C = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 1]
        ], dtype=np.float32)
        nabla_q = np.dot(A, B.T) + np.dot(C, nabla_x.T)
        self.torque = np.dot(self.gradient_coeffs, nabla_q)[0]

    def set_state(self, new_state):
        (q1, q2, q1dot, q2dot) = new_state
        self.input = np.array([
            cos(q1),
            sin(q1),
            q1dot,
            cos(q2),
            sin(q2),
            q2dot
        ], dtype=np.float32)

    def get_torque(self):
        self.compute_control()
        return self.torque

