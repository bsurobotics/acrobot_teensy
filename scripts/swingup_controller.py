#!/usr/bin/env python3

import rospy
import numpy as np
import ml_esc
import lowpass
from std_msgs.msg import Float32, Int16
from sensor_msgs.msg import JointState
from math import pi, sin, cos, atan
from numpy import sign, clip, random

class Acrobot(object):

    def __init__(
        self, m1=2.70125, m2=0.405, lc1=0.18645, lc2=0.177,
        l1=0.6, l2=0.532, I1=0.209, I2=0.01497763, b1=0.05, b2=0.0096,
        # alpha=1.2, kswing_p=60.0, kswing_d=0.2
        alpha=1.28, kswing_p=50.0, kswing_d=0.1
    ):
        self.theta = [0.0, 0.0]
        self.theta_dot = [0.0, 0.0]
        self.theta_dot_motor = 0.0
        self.backlash_valid = [False, False, False, False, False, False, False, False, False, False]
        self.tau = 0.0
        self.m1 = m1
        self.m2 = m2
        self.lc1 = lc1
        self.lc2 = lc2
        self.b1 = b1
        self.b2 = b2
        self.l1 = l1
        self.l2 = l2
        self.I1 = m1*lc1**2 + I1
        self.I2 = m2*lc2**2 + I2
        self.sys_params = (m1, m2, lc1, lc2, b1, b2, l1, l2, m1*lc1**2 + I1, m2*lc2**2 + I2) 
        self.swing_params = (alpha, kswing_p, kswing_d)
        # self.K = 1.0*np.array([-1233.830740237201, -111.9750554715065, -322.69631112860225, -35.89515422709883])
        # self.S = np.array([
        #     [10199.497729865814, 968.374695848282, 2664.83659606605, 333.7350235271134],
        #     [968.3746958415333, 92.96537899193818, 253.0367387536484, 31.694322882566393],
        #     [2664.836596066316, 253.03673875543808, 696.384257998988, 87.21253219449623],
        #     [333.7350235271904, 31.694322882794626, 87.21253219450765, 10.926411242897895]
        # ])
        self.K = 1.0*np.array([-99.28965608639191, -8.698469824075062, -25.88600301535473, -2.8998703841940596])
        self.S = np.array([
            [131118.5813485407, 13188.619849807112, 34319.09727384148, 4126.426248616044], 
            [13188.61984980711, 1341.6103149260966, 3453.1920041273956, 417.62853860707474], 
            [34319.09727384158, 3453.192004127406, 8982.827919989659, 1080.2610594928544], 
            [4126.4262486161015, 417.62853860708054, 1080.2610594928663, 130.31089130481826]
        ])
        self.ml_control = ml_esc.ML_ESC([6, 128, 128, 1])
        self.ml_control.load_weights()
        self.cmd = 0.0

    def update_states(self, jstate):
        pos = jstate.position
        vel = jstate.velocity
        self.theta[0] = pos[0]
        self.theta[1] = pos[2]
        self.theta_dot[0] = vel[0]
        self.theta_dot[1] = vel[2]
        self.theta_dot_motor = vel[1]
        self.backlash_valid.pop(0)
        if vel[1]*vel[2] > 0:
            self.backlash_valid.append(True)
        else:
            self.backlash_valid.append(False)
        self.ml_control.set_state(np.array([pos[0], pos[2], vel[0], vel[2]], dtype=np.float32))

    def compute_linear_control(self):
        (q1, q2) = self.theta
        (q1dot, q2dot) = self.theta_dot
        q1_ = q1 - 2*np.pi*np.around( q1/(2*np.pi) )
        tau = np.dot(
            -self.K, 
            np.array([q1_+pi if q1_<0 else q1_-pi, 1-cos(q2), q1dot, q2dot])
        )
        self.tau = tau
        return tau

    def compute_swing_control(self):
        # Dynamics
        (m1, m2, lc1, lc2, b1, b2, l1, l2, I1, I2) = self.sys_params
        (q1, q2) = self.theta
        (q1dot, q2dot) = self.theta_dot
        (s1, s2, c1, c2) = (sin(q1), sin(q2), cos(q1), cos(q2))
        d11 = I1 + I2 + m2*l1**2 + 2*m2*l1*lc2*c2
        d12 = d21 = I2 + m2*l1*lc2*c2
        d22 = I2
        h1 = -m2*l1*lc2*s2*(2*q1dot*q2dot + q2dot**2)
        h2 = m2*l1*lc2*s2*q1dot**2
        phi1 = (m1*lc1 + m2*l1)*9.81*s1 + m2*lc2*9.81*sin(q1+q2)
        phi2 = m2*lc2*9.81*sin(q1+q2)

        # Collocated PFL (Sigma_2 in paper)
        d22bar = d22 - d21*d12/d11
        h2bar = h2 - d21*h1/d11
        phi2bar = phi2 - d21*phi1/d11

        ## Desired dynamics (Spong's paper)
        # (alpha, kswing_p, kswing_d) = self.swing_params
        # q2d = alpha*atan(q1dot)
        # v2 = kswing_p*(q2d - q2) - kswing_d*q2dot
        ## Desired dynamics (Russ's notes)
        E_current = 0.5*(d11*q1dot**2 + 2*d12*q1dot*q2dot + d22*q2dot**2) - (m1*lc1 + m2*l1)*9.81*c1 - m2*lc2*9.81*cos(q1+q2)
        E_ref = -(m1*lc1 + m2*l1)*9.81*cos(pi) - m2*lc2*9.81*cos(pi)
        ubar = q1dot * (E_current - E_ref)
        (k1, k2, k3) = (0.0, 0.0, 4.0)
        v2 = -k1*q2 - k2*q2dot + k3*ubar

        tau = d22bar*v2 + h2bar + phi2bar
        self.tau = tau
        return tau
        # return -0.004*q2dot*(E_current - E_ref)

    def compute_ml_control(self):
        tau = self.ml_control.get_torque()
        self.tau = tau
        return tau

    def compute_friction_compensation(self):
        q1dot = self.theta_dot[0]
        if abs(q1dot) < 0.1:
            return -0.5*atan(10.0*q1dot)/pi*2
        else:
            return 0.0

    def isin_lqr_roa(self):
        (q1, q2) = self.theta
        (q1dot, q2dot) = self.theta_dot
        q1_ = q1 - 2*np.pi*np.around( q1/(2*np.pi) )
        # x = np.array([q1_+pi if q1_<0 else q1_-pi, 1-cos(q2), q1dot, q2dot])
        x = np.array([q1+pi if q1<0 else q1-pi, q2, q1dot, q2dot])
        return (np.dot(x.T, np.dot(self.S, x))) <= 100.0     # 8.0 is experimentally found

def run_ros_node():

    ## Initialize ROS node
    rospy.init_node('acrobot_swingup_controller', anonymous=True)

    ## Create Acrobot instance
    robot = Acrobot()

    ## Read ROS params
    state_topic_name = rospy.get_param("~state_topic", "acrobot_state")
    effort_topic_name = rospy.get_param("~effort_topic", "acrobot_command")
    deadband = rospy.get_param("~deadband", 0.0)
    max_torque = rospy.get_param("~saturation", 2.5)
    publish_rate = rospy.get_param("~publish_rate", 200)
    rospy.loginfo("[swingup_controller] Publishing torque commands to: %s",     effort_topic_name)
    rospy.loginfo("[swingup_controller] Deadband:                      %f N-m", deadband)
    rospy.loginfo("[swingup_controller] Saturation:                    %f N-m", max_torque)
    rospy.loginfo("[swingup_controller] Publish rate:                  %d Hz",  publish_rate)

    ## Setup pub/sub
    pub = rospy.Publisher(effort_topic_name, Float32, queue_size=1)
    pub_filtered = rospy.Publisher(effort_topic_name + "_filtered", Float32, queue_size=1)
    jstate_sub = rospy.Subscriber(state_topic_name, JointState, robot.update_states)
    rate = rospy.Rate(publish_rate)

    ## Filter command to prevent shattering
    lpf = lowpass.LowPassFilter(omega=0.33, T=1.0/publish_rate)

    while not rospy.is_shutdown():
        # if False:
        if robot.isin_lqr_roa():
            torque = robot.compute_linear_control()
        else:
            # torque = 0.0
            torque = robot.compute_ml_control()
            # torque = [
            #     robot.compute_ml_control()*1.0, 
            #     robot.compute_swing_control()*-0.0,
            #     robot.compute_friction_compensation()*0.0
            # ]

        if all(robot.backlash_valid):
            if torque < 0:
                # cmd = torque
                cmd = clip(torque, -max_torque, -deadband) 
            elif torque > 0:
                # cmd = torque
                cmd = clip(torque, deadband, max_torque)
            else:
                cmd = 0.0
            robot.cmd = cmd
        else:
            cmd = robot.cmd
        pub.publish(cmd)
        pub_filtered.publish(lpf.filter(cmd))
        # pub.publish(torque[0])
        # pub_filtered.publish(torque[1])
        
        rate.sleep()


if __name__ == '__main__':
    try:
        run_ros_node()
    except rospy.ROSInterruptException:
        pass
    
