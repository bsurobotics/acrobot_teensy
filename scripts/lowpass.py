class LowPassFilter(object):

    def __init__(self, omega=7.0, T=1.0/450.0):
        self.cutOffFrequency = omega
        self.samplePeriod = T
        self.lastState = 0.0
        self.lastStateDerivative = 0.0
        self.wT = omega*T

    def setFilterParameters(self, omega, T):
        self.cutOffFrequency = omega
        self.samplePeriod = T
        self.wT = omega*T

    def filter(self, currentState):
        stateDerivative = (2.0 - self.wT)/(2.0 + self.wT)*self.lastStateDerivative + \
                               (2.0*self.cutOffFrequency)/(2.0 + self.wT)*(currentState - self.lastState)
        self.lastState = currentState
        self.lastStateDerivative = stateDerivative
        return stateDerivative
