#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32, Int16
from sensor_msgs.msg import JointState
from math import pi, sin
from numpy import sign, clip, random


def talker():
    rospy.init_node('periodic_torque', anonymous=True)

    effort_topic_name = rospy.get_param("~effort_topic", "acrobot_command")
    amplitude = rospy.get_param("~amplitude", 0.1)
    frequency = rospy.get_param("~frequency", 1)
    deadband = rospy.get_param("~deadband", 0)
    max_torque = rospy.get_param("~saturation", 1.0)
    publish_rate = rospy.get_param("~publish_rate", 200)
    signal_type = rospy.get_param("~signal_type", "sine")

    rospy.loginfo("[periodic_torque] Publishing torque commands to: %s", effort_topic_name)
    rospy.loginfo("[periodic_torque] Amplitude:                     %f N-m", amplitude)
    rospy.loginfo("[periodic_torque] Frequency:                     %f Hz", frequency)
    rospy.loginfo("[periodic_torque] Deadband:                      %f N-m", deadband)
    rospy.loginfo("[periodic_torque] Saturation:                    %f N-m", max_torque)
    rospy.loginfo("[periodic_torque] Signal type:                   %s", signal_type)
    rospy.loginfo("[periodic_torque] Publish rate:                  %d Hz", publish_rate)

    pub = rospy.Publisher(effort_topic_name, Float32, queue_size=1)
    rate = rospy.Rate(publish_rate)

    while not rospy.is_shutdown():
        torque = compute_torque(amplitude, frequency, type=signal_type)
        cmd = 0
        if torque < 0:
            cmd = clip(torque, -max_torque, -deadband) 
        elif torque > 0:
            cmd = clip(torque, deadband, max_torque)
        pub.publish(cmd)
        rate.sleep()

def compute_torque(amplitude, frequency, type="sine"):
    torque = 0.0
    if type == "sine":
        torque = amplitude * sin(rospy.get_time()*2*pi*frequency)
    elif type == "square":
        torque = amplitude * sign( sin(rospy.get_time()*2*pi*frequency) )
    elif type == "constant":
        torque = amplitude
    elif type == "dithering":
        torque = amplitude + 0.1*amplitude*sin(rospy.get_time()*2*pi*frequency) 
    return torque

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
    
